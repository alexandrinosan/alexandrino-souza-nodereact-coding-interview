import axios from "axios";
import { IUserProps, IPagination } from "../dtos/user.dto";

export class BackendClient {
  private readonly baseUrl: string;

  constructor(baseUrl = "http://localhost:3001/v1") {
    this.baseUrl = baseUrl;
  }

  async getAllUsers(
    offset: number,
    limit: number
  ): Promise<{ data: IUserProps[]; pagination: IPagination }> {
    return (
      await axios.get(
        `${this.baseUrl}/people/all?offset=${offset}&limit=${limit}`,
        {}
      )
    ).data;
  }
}

export interface IUserProps {
  id: string;
  avatar: string;
  first_name: string;
  last_name: string;
  email: string;
  gender: string;
  title?: string;
  company?: string;
}

export interface IPagination {
  total: number;
  offset: number;
  limit: number;
}

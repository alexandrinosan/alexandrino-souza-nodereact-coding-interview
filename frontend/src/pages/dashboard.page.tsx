import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";
import { Pagination } from "@mui/material";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [offset, setOffset] = useState<number>(0);
  const [pageNumber, setPageNumber] = useState<number>(1);
  const [pagination, setPagination] = useState({
    total: 0,
    offset: 0,
    limit: 0,
  } as any);

  const limit = 10;

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      const { data, pagination } = await backendClient.getAllUsers(
        offset,
        limit
      );
      setPagination(pagination);
      setUsers(data);
      setLoading(false);
    };

    fetchData();
  }, [offset]);

  const handleChangePage = (
    event: React.ChangeEvent<unknown>,
    value: number
  ) => {
    if (value !== pageNumber) {
      setPageNumber(value);
      setOffset(value - 1);
    }
  };

  return (
    <div style={{ paddingTop: "30px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            {users.length
              ? users.map((user) => {
                  return <UserCard key={user.id} {...user} />;
                })
              : null}
          </div>
        )}
      </div>
      <div style={{ width: "50%", margin: "30px auto" }}>
        <Pagination
          count={pagination.total}
          page={pageNumber}
          onChange={handleChangePage}
        />
      </div>
    </div>
  );
};

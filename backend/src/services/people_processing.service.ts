import people_data from "../data/people_data.json";

export class PeopleProcessing {
  getById(id: number) {
    return people_data.find((p) => p.id === id);
  }

  getAll(offset: number = 0, limit: number = 10) {
    const start = offset * limit;
    const end = start + limit;
    return {
      data: people_data.slice(start, end),
      pagination: {
        total: Math.ceil(people_data.length / limit),
        offset,
        limit,
      },
    };
  }
}
